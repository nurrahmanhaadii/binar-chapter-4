package id.haadii.binar.chapter4.day1

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import id.haadii.binar.chapter4.R
import id.haadii.binar.chapter4.databinding.FragmentDay1Binding


class Day1Fragment : Fragment() {
    private var _binding: FragmentDay1Binding? = null
    private val binding get() = _binding!!

    private lateinit var snackbar2: Snackbar

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDay1Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showToast()
        snackbar2 = Snackbar.make(binding.btnSnackBar, "Button SnackBar telah di tekan!", Snackbar.LENGTH_INDEFINITE)
        showSnackBar()
        showSnackBarWithAction()
        hideSnackBar()
    }

    private fun showToast() {
        binding.btnToast.setOnClickListener {
            createToast("Button Toast telah di tekan!").show()
        }
    }

    private fun showSnackBar() {
        binding.btnSnackBar.setOnClickListener {
            snackbar2.show()
        }
    }

    private fun hideSnackBar() {
        binding.btnSnackBarDismiss.setOnClickListener {
            if (snackbar2.isShown) {
                snackbar2.dismiss()
            }
        }
    }

    private fun showSnackBarWithAction() {
        binding.btnSnackBarAction.setOnClickListener {
            customColorSnackBar(it, "Button SnackBar dengan Action di tekan!")
                .setAction(getString(R.string.txt_take_action)) {
                    createToast("Take action di tekan!").show()
                }
                .show()
        }
    }

    private fun createToast(message: String): Toast {
        return Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT)
    }

    private fun createSnackBar(it: View, message: String): Snackbar {
        return Snackbar.make(it, message, Snackbar.LENGTH_INDEFINITE)
    }

    private fun customColorSnackBar(it: View, message: String) : Snackbar {
        val snackbar = createSnackBar(it, message)
        snackbar.setBackgroundTint(ContextCompat.getColor(requireContext(), R.color.purple_500))
            .setActionTextColor(ContextCompat.getColor(requireContext(), R.color.black))
            .setTextColor(ContextCompat.getColor(requireContext(), R.color.teal_200))
        return snackbar
    }

}