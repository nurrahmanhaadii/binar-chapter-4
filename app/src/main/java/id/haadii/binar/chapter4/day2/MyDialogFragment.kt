package id.haadii.binar.chapter4.day2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import id.haadii.binar.chapter4.R
import id.haadii.binar.chapter4.databinding.FragmentMyDialogBinding

class MyDialogFragment : DialogFragment() {

    private var _binding: FragmentMyDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMyDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
//        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            titleDialogFragment.text = "getString(R.string.title)"
            descriptionDialogFragment.text = "getString(R.string.description)"
            btnDialogfragment.text = "getString(R.string.btn_dismiss)"
        }

        binding.btnDialogfragment.setOnClickListener {
            requireDialog().dismiss()
            Toast.makeText(requireContext(), "Berhasil Dismiss", Toast.LENGTH_SHORT).show()
        }
    }
}
