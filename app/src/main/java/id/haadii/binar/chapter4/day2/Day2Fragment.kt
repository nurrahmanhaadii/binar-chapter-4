package id.haadii.binar.chapter4.day2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import id.haadii.binar.chapter4.R
import id.haadii.binar.chapter4.databinding.FragmentDay2Binding

class Day2Fragment : Fragment() {
    private var _binding: FragmentDay2Binding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDay2Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showAlertDialog()
        showAlertDialogWithAction()
        showAlertDialogCustom()
        showAlertDialogFragment()
    }

    private fun showAlertDialog() {
        binding.btnDialog.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle("Disini Judul")
            dialog.setMessage("Disini pesan, boleh panjang panjang karena memang untuk memberi pesan. Kalau judul pendek aja")
            dialog.show()
        }
    }

    private fun showAlertDialogWithAction() {
        binding.btnDialogAction.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle("Disini Judul")
            dialog.setMessage("Disini pesan, boleh panjang panjang karena memang untuk memberi pesan. Kalau judul pendek aja")
            dialog.setPositiveButton("Positif") { dialogInterface, angka ->
                createToast("Ini Button Positif").show()
            }
            dialog.setNeutralButton("Netral") { dialogInterface, _ ->

            }
            dialog.setNegativeButton("Negatif") { dialogInterface, _ ->

            }
            dialog.setCancelable(false)
            dialog.show()
        }
    }

    private fun showAlertDialogCustom() {
        // Menghubungkan layout
        val customLayout = LayoutInflater.from(requireContext()).inflate(R.layout.layout_alert_dialog, null, false)

        // Memanggil text view dan button dari layout custom
        val title = customLayout.findViewById<TextView>(R.id.tv_title)
        val buttonOne = customLayout.findViewById<Button>(R.id.btn_one)

        // Mengubah value dari text view dan button
        title.text = "Ini sudah di ubah"
        buttonOne.text = "Dismiss"

        // Membuat builder alert dialog
        val builder = AlertDialog.Builder(requireContext())

        // Mengubah layout Alert dialog menggunakan custom layout
        builder.setView(customLayout)

        // Membuat alert dialog baru dari builder yang sudah di custom layout
        val dialog = builder.create()

        // Action pada button custom layout
        buttonOne.setOnClickListener {
            dialog.dismiss()
        }

        binding.btnDialogCustom.setOnClickListener {
            dialog.show()
        }
    }

    private fun showAlertDialogFragment() {
        binding.btnDialogFragment.setOnClickListener {
            val myDialog = MyDialogFragment()
            myDialog.isCancelable = false
            myDialog.show(childFragmentManager, null)
        }
    }

    private fun createToast(message: String): Toast {
        return Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT)
    }

}