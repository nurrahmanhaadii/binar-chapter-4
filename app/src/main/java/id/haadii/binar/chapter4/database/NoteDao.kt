package id.haadii.binar.chapter4.database

import androidx.room.*


/**
 * Created by nurrahmanhaadii on 07,April,2022
 */
@Dao
interface NoteDao {
    /**
     * kita harus dapetin note sesuai dengan user login
     *
     */
    @Query("SELECT * FROM Note WHere student_id = :studentId")
    fun getAllNote(studentId: Int) : List<Note>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNote(note: Note) : Long

    @Update
    fun updateNote(note: Note) : Int

    @Delete
    fun deleteNote(note: Note) : Int
}