package id.haadii.binar.chapter4.helper

import android.content.Context
import id.haadii.binar.chapter4.database.Note
import id.haadii.binar.chapter4.database.Student
import id.haadii.binar.chapter4.database.StudentDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


/**
 * Created by nurrahmanhaadii on 07,April,2022
 */
class StudentRepo(context: Context) {

    private val mDb = StudentDatabase.getInstance(context)

    suspend fun getDataStudent() = withContext(Dispatchers.IO) {
        mDb?.studentDao()?.getAllStudent()
    }

    suspend fun updateStudent(student: Student) = withContext(Dispatchers.IO) {
        mDb?.studentDao()?.updateStudent(student)
    }

    suspend fun insertStudent(student: Student) = withContext(Dispatchers.IO) {
        mDb?.studentDao()?.insertStudent(student)
    }

    suspend fun deleteStudent(student: Student) = withContext(Dispatchers.IO) {
        mDb?.studentDao()?.deleteStudent(student)
    }

    /**
     * Note
     */

    suspend fun insertNote(student: Student, note: Note) = withContext(Dispatchers.IO) {
        val note = Note(id = null, studentId = student.id ?: 0, note = note.note)
        mDb?.noteDao()?.insertNote(note)
    }

    suspend fun updatetNote(student: Student, note: Note) = withContext(Dispatchers.IO) {
        val note = Note(id = null, studentId = student.id ?: 0, note = note.note)
        mDb?.noteDao()?.updateNote(note)
    }

    suspend fun deletetNote(student: Student, note: Note) = withContext(Dispatchers.IO) {
        val note = Note(id = null, studentId = student.id ?: 0, note = note.note)
        mDb?.noteDao()?.deleteNote(note)
    }

    suspend fun getNote(student: Student) = withContext(Dispatchers.IO) {
        mDb?.noteDao()?.getAllNote(studentId = student.id ?: 0)
    }

    /**
     * Kita perlu balikan data dari database
     * yang di tampung di dalam function
     */

    fun getData(): List<Student> {
        var result = emptyList<Student>()
        CoroutineScope(Dispatchers.IO).launch {
            result = mDb?.studentDao()?.getAllStudent()!!
        }
        return result
    }
}