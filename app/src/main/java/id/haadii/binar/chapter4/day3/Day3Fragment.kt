package id.haadii.binar.chapter4.day3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import id.haadii.binar.chapter4.R
import id.haadii.binar.chapter4.adapter.ContactAdapter
import id.haadii.binar.chapter4.databinding.FragmentDay3Binding
import id.haadii.binar.chapter4.model.Contact


class Day3Fragment : Fragment() {
    private var _binding: FragmentDay3Binding? = null
    private val binding get() = _binding!!

    private lateinit var contactAdapter: ContactAdapter

    private val listContacts = arrayListOf<Contact>(
        Contact("orang 1", "081234", R.drawable.ic_baseline_access_time_24),
        Contact("orang 2", "091234", R.drawable.ic_baseline_access_time_24),
        Contact("orang 3", "010000", R.drawable.ic_baseline_access_time_24)
    )

    private val listContactsUpdate = arrayListOf<Contact>(
        Contact("orang 1", "081234", R.drawable.ic_baseline_access_time_24),
        Contact("orang 2", "9999999", R.drawable.ic_baseline_account_circle_24),
        Contact("orang 3", "010000", R.drawable.ic_baseline_access_time_24)
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDay3Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        buttonUpdateClicked()
    }

    private fun initRecyclerView() {
        contactAdapter = ContactAdapter()

        // Cara lama
//        contactAdapter.updateData(listContacts)

        // Cara baru
        updateWithDiffUtil(listContacts)
        binding.rvData.apply {
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = contactAdapter
        }
    }

    private fun buttonUpdateClicked() {
        binding.btnUpdate.setOnClickListener {
            // Cara lama
//            updateData(listContactsUpdate)

            // Cara baru
            updateWithDiffUtil(listContactsUpdate)
        }
    }

    private fun updateData(value: ArrayList<Contact>) {
        contactAdapter.updateData(value)
        contactAdapter.notifyDataSetChanged()
    }

    private fun updateWithDiffUtil(value: ArrayList<Contact>) {
        contactAdapter.submitData(value)
    }

}