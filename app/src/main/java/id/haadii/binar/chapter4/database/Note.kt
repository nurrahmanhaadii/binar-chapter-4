package id.haadii.binar.chapter4.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


/**
 * Created by nurrahmanhaadii on 07,April,2022
 */
@Entity
data class Note(
    @PrimaryKey(autoGenerate = true) val id : Int?,
    @ColumnInfo(name = "student_id") val studentId: Int,
    @ColumnInfo(name = "note") val note: String
)
