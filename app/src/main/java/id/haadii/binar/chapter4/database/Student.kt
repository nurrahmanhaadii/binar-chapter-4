package id.haadii.binar.chapter4.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


/**
 * Created by nurrahmanhaadii on 04,April,2022
 */
@Entity
data class Student(
    @PrimaryKey(autoGenerate = true) val id : Int?,
    @ColumnInfo(name = "nama") val nama: String,
    @ColumnInfo(name = "email") val email: String
)
