package id.haadii.binar.chapter4.helper

import android.content.Context


/**
 * Created by nurrahmanhaadii on 04,April,2022
 */
class SharedPref(context: Context) {

    private val sharedPref = context.getSharedPreferences("ini_id", Context.MODE_PRIVATE)

    fun setData() {
        val editor = sharedPref.edit()
        editor.putString("username", "disini usernmae user")
        editor.putInt("userid", 1)
        editor.putBoolean("islogin", true)
        editor.apply()
    }

    fun getData() {

    }
}