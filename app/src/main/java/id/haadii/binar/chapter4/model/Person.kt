package id.haadii.binar.chapter4.model


/**
 * Created by nurrahmanhaadii on 30,March,2022
 */
data class Person(
    val nama: String,
    val alamat: String
)
