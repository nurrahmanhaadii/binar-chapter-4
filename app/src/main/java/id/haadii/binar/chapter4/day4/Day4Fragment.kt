package id.haadii.binar.chapter4.day4

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import id.haadii.binar.chapter4.R
import id.haadii.binar.chapter4.databinding.FragmentDay4Binding

class Day4Fragment : Fragment() {
    private var _binding: FragmentDay4Binding? = null
    private val binding get() = _binding!!

    // Ini buat multiple shared preference, di bedakan dari Id
    private lateinit var sharedPref: SharedPreferences

    // Ini buat satu shared preference
//    private val singleSharedPref = requireActivity().getPreferences(Context.MODE_PRIVATE).edit()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDay4Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPref = requireContext().getSharedPreferences("ini_id", Context.MODE_PRIVATE)

        btnSaveClicked()
        btnViewClicked()
        btnClearClicked()
        btnMoveClicked()
    }

    private fun btnSaveClicked() {
        binding.btnSave.setOnClickListener {
            // Dapetin value dari edit text
            val id = binding.etInputId.text.toString().toInt()
            val nama = binding.etInputName.text.toString()

            // Bikin editor
            val editor = sharedPref.edit()

            // Narok value kedalam shared pref
            editor.putInt("id", id)
            editor.putString("nama", nama)

            // nyimpan ke shared pref
            editor.apply()

            Toast.makeText(requireContext(), "Berhasil disimpan", Toast.LENGTH_SHORT).show()
        }
    }

    private fun btnViewClicked() {
        binding.apply {
            btnView.setOnClickListener {
                // Dapetin value dari shared preference
                val id = sharedPref.getInt("id", 123)
                val nama = sharedPref.getString("nama", "default value")

                // Munculin ke dalam text view
                tvShowId.text = id.toString()
                tvShowName.text = nama.toString()
            }
        }
    }

    private fun btnClearClicked() {
        binding.apply {
            btnClear.setOnClickListener {
                // Bikin editor
                val editor = sharedPref.edit()

                // Clear data
                editor.clear()

                // Save
                editor.apply()

                // hapus data dari Text View
                tvShowId.text = ""
                tvShowName.text = ""
            }
        }
    }

    private fun btnMoveClicked() {
        binding.btnMove.setOnClickListener {
            findNavController().navigate(R.id.action_day4Fragment_to_day5Fragment)
        }
    }

}