package id.haadii.binar.chapter4.model


/**
 * Created by nurrahmanhaadii on 30,March,2022
 */
data class Contact(
    val nama: String,
    val phone: String,
    val image: Int
)
