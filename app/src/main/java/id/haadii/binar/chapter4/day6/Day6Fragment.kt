package id.haadii.binar.chapter4.day6

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import id.haadii.binar.chapter4.R
import id.haadii.binar.chapter4.adapter.StudentActionListener
import id.haadii.binar.chapter4.adapter.StudentAdapter
import id.haadii.binar.chapter4.database.Student
import id.haadii.binar.chapter4.database.StudentDatabase
import id.haadii.binar.chapter4.databinding.FragmentDay6Binding
import id.haadii.binar.chapter4.helper.StudentRepo
import kotlinx.coroutines.*

class Day6Fragment : Fragment() {
    private var _binding: FragmentDay6Binding? = null
    private val binding get() = _binding!!

    private lateinit var studentAdapter: StudentAdapter
    private lateinit var studentRepo: StudentRepo
    private var mDb: StudentDatabase? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentDay6Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mDb = StudentDatabase.getInstance(requireContext())
        studentRepo = StudentRepo(requireContext())
        initRecyclerView()
        getDataFromDb()
        addStudent()
    }

    private fun initRecyclerView() {
        binding.apply {
            studentAdapter = StudentAdapter({}, {}, action)
            rvData.adapter = studentAdapter
            rvData.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun addStudent() {
        binding.fabAdd.setOnClickListener {
            showAlertDialog(null, true)
        }
    }

    private fun showAlertDialog(student: Student?, isFromButtonAdd: Boolean) {
        val customLayout = LayoutInflater.from(requireContext()).inflate(R.layout.layout_dialog_add, null, false)

        val etName = customLayout.findViewById<EditText>(R.id.et_name)
        val etEmail = customLayout.findViewById<EditText>(R.id.et_email)
        val btnSave = customLayout.findViewById<Button>(R.id.btn_save)

        if (!isFromButtonAdd && student != null) {
            etName.setText(student.nama)
            etEmail.setText(student.email)
            btnSave.text = "Update"
        }

        val builder = AlertDialog.Builder(requireContext())

        builder.setView(customLayout)

        val dialog = builder.create()

        btnSave.setOnClickListener {
            val nama = etName.text.toString()
            val email = etEmail.text.toString()
            if (student != null) {
                val newStudent = Student(student.id, nama, email)
                updateToDb(newStudent)
            } else {
                saveToDb(nama, email)
//                checkRegisteredStudent(nama, email)
            }
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun showAlertDialogAdd() {
        createCustomDialog { nama, email ->
            saveToDb(nama, email)
        }
    }

    private fun showAlertDialogUpdate(student: Student) {
        createCustomDialog { nama, email ->
            val newStudent = Student(student.id, nama, email)
            updateToDb(newStudent)
        }
    }

    private fun createCustomDialog(onClickListener: (String, String) -> Unit) {
        val customLayout = LayoutInflater.from(requireContext()).inflate(R.layout.layout_dialog_add, null, false)

        val etName = customLayout.findViewById<EditText>(R.id.et_name)
        val etEmail = customLayout.findViewById<EditText>(R.id.et_email)
        val btnSave = customLayout.findViewById<Button>(R.id.btn_save)

        val builder = AlertDialog.Builder(requireContext())

        builder.setView(customLayout)

        val dialog = builder.create()

        btnSave.setOnClickListener {
            onClickListener.invoke(etName.text.toString(), etEmail.text.toString())
        }

        dialog.show()

    }

    private fun saveToDb(name: String, email: String) {
        val student = Student(null, name, email)
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDb?.studentDao()?.insertStudent(student)
            if (result != 0L) {
                getDataFromDb()
                showToastInMainThread(getString(R.string.succeeded_add))
            } else {
                showToastInMainThread("Gagal Ditambahkan")
            }
        }
    }

    private fun showToastInMainThread(message: String) {
        CoroutineScope(Dispatchers.Main).launch {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * 1. validasi
     * 2. pindah ke halman home
     * 3. wajib bawa nilai dari halaman login ke halaman home
     */

    private fun checkRegisteredStudent(name: String, email: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDb?.studentDao()?.getRegisteredStudent(name, email)
            if (!result.isNullOrEmpty()) {
                result[0].id
                result[0].nama
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Student telah ada", Toast.LENGTH_SHORT).show()
                }
            } else {
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Student belum ada", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun getDataFromDb() {
        CoroutineScope(Dispatchers.IO).launch {
            val result2 = studentRepo.getDataStudent()
            if (result2 != null) {
                CoroutineScope(Dispatchers.Main).launch {
                    studentAdapter.updateData(result2)
                }
            }
        }
    }

    private fun updateToDb(student: Student) {
        CoroutineScope(Dispatchers.IO).launch {
            val result = studentRepo.updateStudent(student)
            if (result != 0) {
                getDataFromDb()
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Berhasil Diupdate", Toast.LENGTH_SHORT).show()
                }
            } else {
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Gagal Diupdate", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun deleteItemDb(student: Student) {
        CoroutineScope(Dispatchers.IO).launch {
            val result = studentRepo.deleteStudent(student)
            if (result != 0) {
                getDataFromDb()
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Berhasil Dihapus", Toast.LENGTH_SHORT).show()
                }
            } else {
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Gagal Dihapus", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private val action = object : StudentActionListener {
        override fun onDelete(student: Student) {
            deleteItemDb(student)
        }

        override fun onEdit(student: Student) {
            showAlertDialog(student, false)
        }

    }

}