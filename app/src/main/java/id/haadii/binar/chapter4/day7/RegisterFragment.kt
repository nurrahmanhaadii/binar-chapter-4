package id.haadii.binar.chapter4.day7

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.navigation.fragment.findNavController
import id.haadii.binar.chapter4.R
import id.haadii.binar.chapter4.databinding.FragmentRegisterBinding

class RegisterFragment : Fragment() {
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkConfirmPasswordOnChange()
        checkByClickButton()
    }

    private fun checkConfirmPasswordOnChange() {
        binding.etConfirmPass.addTextChangedListener(textWatcher)
        binding.etConfirmPass.doAfterTextChanged {

        }
    }

    private val textWatcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun afterTextChanged(p0: Editable?) {
            if (checkConfirmPassword() || p0.isNullOrEmpty()) {
                binding.tilConfirmPass.error = null
            } else {
                binding.tilConfirmPass.error = "tidak sesuai"
            }
        }
    }

    private fun checkConfirmPassword(): Boolean {
        binding.apply {
            return etPass.text.toString() == etConfirmPass.text.toString()
        }
    }

    private fun checkByClickButton() {
        binding.apply {
            btnCheck.setOnClickListener {
                if (checkConfirmPassword()) {
                    Toast.makeText(requireContext(), "Register Berhasil", Toast.LENGTH_SHORT).show()
                    findNavController().popBackStack()
                } else {
                    Toast.makeText(requireContext(), "Register Gagal", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}