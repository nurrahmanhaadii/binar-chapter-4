package id.haadii.binar.chapter4.day5

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import id.haadii.binar.chapter4.R
import id.haadii.binar.chapter4.databinding.FragmentDay5Binding


class Day5Fragment : Fragment() {
    private var _binding: FragmentDay5Binding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPref: SharedPreferences
    private var dataId: Int? = 0
    private var dataName: String? = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentDay5Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        sharedPref = context.getSharedPreferences("ini_id", Context.MODE_PRIVATE)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getData()
        showData()
    }

    private fun getData() {
        dataId = sharedPref.getInt("id", 0)
        dataName = sharedPref.getString("nama", "Ini Default Value")
    }

    private fun showData() {
        binding.apply {
            btnShow.setOnClickListener {
                tvValueId.text = dataId.toString()
                tvValueName.text = dataName
            }
        }
    }

}