package id.haadii.binar.chapter4.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.haadii.binar.chapter4.R
import id.haadii.binar.chapter4.model.Contact


/**
 * Created by nurrahmanhaadii on 30,March,2022
 */
class ContactAdapter()
    : RecyclerView.Adapter<ContactAdapter.ContactViewHolder>() {

    private var listContact = arrayListOf<Contact>()

    private val diffCallback = object : DiffUtil.ItemCallback<Contact>() {
        override fun areItemsTheSame(oldItem: Contact, newItem: Contact): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Contact, newItem: Contact): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }

    }

    private val differ = AsyncListDiffer(this, diffCallback)

    // Cara baru
    fun submitData(items: ArrayList<Contact>) = differ.submitList(items)

    // Cara Lama
    fun updateData(items: ArrayList<Contact>) {
        this.listContact = items
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false)
        return ContactViewHolder(view)
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        // Cara lama
//        holder.bind(listContact[position])

        // Cara Baru
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount(): Int {
        // Cara lama
//        return listContact.size

        // Cara baru
        return differ.currentList.size
    }

    inner class ContactViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val tvNama: TextView = view.findViewById(R.id.tv_nama)
        val tvPhone: TextView = view.findViewById(R.id.tv_phone)
        val ivImage: ImageView = view.findViewById(R.id.iv_image)

        fun bind(item: Contact) {
            tvNama.text = item.nama
            tvPhone.text = item.phone
            ivImage.setImageResource(item.image)

//            Glide.with(itemView.context)
//                .load(R.drawable.ic_launcher_background)
//                .into(ivImage)
        }
    }
}